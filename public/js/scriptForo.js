
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getAuth, createUserWithEmailAndPassword, signInWithEmailAndPassword, onAuthStateChanged, signOut } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js";
import { getDatabase, child, ref, push, update, set, get, remove } from "https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js";

console.log("Inicio del programa")

const firebaseConfig = {
  apiKey: "AIzaSyANnhLz7-ekbH3IHZ0PkvHfd1dsqO-QQyE",
  authDomain: "primer-foro-1bd20.firebaseapp.com",
  projectId: "primer-foro-1bd20",
  storageBucket: "primer-foro-1bd20.appspot.com",
  messagingSenderId: "1063270929475",
  appId: "1:1063270929475:web:5bfa40bac74ac1ab9b70f6",
  measurementId: "G-W2V1R12H41"
};

const app = initializeApp(firebaseConfig);
//Referencias al HTML
var Section = document.getElementById("SectionId");
var textoRef = document.getElementById("textoForoId");
var tituloRef = document.getElementById("TituloForoId");
var botonRef = document.getElementById("buttonForoId");
var botonEditRef = document.getElementById("buttonEditId");
var usuarioRef = document.getElementById("UserLogId");
var WriterRef = document.getElementById("SWrite");

var btnLogIn = document.getElementById("btnlogin");
var btnLogOut = document.getElementById("btnlogout");
// variables
var UsuarioData;
var Posts;
var change = 0;
const auth = getAuth();
const database = getDatabase();
const user = auth.currentUser;
var UserID;
var thekey;
//Escuchadores
btnLogOut.addEventListener("click", LogOut)
botonRef.addEventListener("click", writeNewPost);
botonEditRef.addEventListener("click", FinishEdit);
textoRef.addEventListener("keypress", (e) => {
  if (e.key === 'Enter') {
    if (change == 0) {
      writeNewPost()
    }
    else {
      FinishEdit()
    }
  }
})



botonEditRef.style.display = "none";
botonRef.style.display = "block";
// Cambio de estado en el inicio de secion 
onAuthStateChanged(auth, (user) => {
  if (user) { // https://firebase.google.com/docs/reference/js/firebase.User --> referencia
    console.log("Usuario logeado")
    btnLogIn.style.display = "none";
    btnLogOut.style.display = "block";

    get(ref(database, "users/" + user.uid)).then((snapshot) => {
      if (snapshot.exists()) {
        console.log(snapshot.val());
        UsuarioData = snapshot.val().UserData
        console.log(UsuarioData.Name + " de " + UsuarioData.city)
        UserID = UsuarioData.uid
        usuarioRef.appendChild(document.createTextNode(UsuarioData.Name + " "))
        GetDataBase()
      } else {
        console.log("No data available");
      }
    }).catch((error) => {
      console.error(error);
    });

  }
  else {
    console.log("Usuario no logeado")
    btnLogIn.style.display = "block";
    btnLogOut.style.display = "none";
    WriterRef.innerHTML = ""
    GetDataBase()

  }
});

//Gestion del post
function LogOut() {
  auth.signOut()
  location.reload(true)
}
function GetDataBase() {
  get(ref(database, "posts/")).then((snapshot) => {
    if (snapshot.exists()) {
      console.log(snapshot.val());
      Posts = snapshot.val()
      console.log(Posts)
      for (const i in Posts) {
        console.log(Posts[i].uid)
        let uid = Posts[i].uid
        Post(Posts[i].title, Posts[i].body, Posts[i].author, Posts[i].city, i, uid)
      }
    } else {
      console.log("No data available");
    }
  }).catch((error) => {
    console.error(error);
  });

}
  // funcion escribir un nuevo post
function writeNewPost() {
  console.log("Boton Apretado")

  const postData = {
    author: UsuarioData.Name,
    body: textoRef.value,
    title: tituloRef.value,
    city: UsuarioData.city,
    uid: UsuarioData.uid,
  };
  const newPostKey = push(child(ref(database), 'posts')).key;
  Post(tituloRef.value, textoRef.value, UsuarioData.Name, UsuarioData.city, newPostKey, UsuarioData.uid)
  tituloRef.value = ""
  textoRef.value = ""
  const updates = {};
  updates['/posts/' + newPostKey] = postData;

  return update(ref(database), updates)
};
// Post configuracion 
function Post(titulo, cuerpo, nombre, ciudad, key, Userid) {
  var tag = document.createElement("div");
  tag.classList.add('post');
  var title = document.createElement("h3");
  var text = document.createTextNode(cuerpo);
  var titext = document.createTextNode(titulo + "| Comentario de: " + nombre + " (" + ciudad + ")]")
  title.appendChild(titext);
  tag.appendChild(title);
  tag.appendChild(text);
  console.log(UserID);
  if (Userid == UserID) {
    var botonE = document.createElement("i");
    botonE.classList.add('bi', 'bi-pencil-fill');
    var botonC = document.createElement("i");
    botonC.classList.add('bi', 'bi-trash3');
    var btextE = document.createTextNode("Editar");
    var btextC = document.createTextNode("Eliminar");
    tag.appendChild(botonE)
    tag.appendChild(botonC)
    botonE.addEventListener("click", (e) => {
      console.log("1");
      EditPost(key);
    })
    botonC.addEventListener("click", (e) => {
      ClearPost(key);
      console.log("2");
    });
  }

  Section.prepend(tag);
}
//funcion editar el post
function EditPost(Postkey) {
  thekey = Postkey
  console.log("EDITANDO post: " + thekey)
  get(ref(database, "posts/" + thekey)).then((snapshot) => {
    if (snapshot.exists()) {
      console.log(snapshot.val());
      Posts = snapshot.val()
      textoRef.value = Posts.body
      tituloRef.value = Posts.title
      botonRef.style.display = "none";
      botonEditRef.style.display = "block";
      change = 1
    }
    else {
      console.log("No data available");
    }
  }).catch((error) => {
    console.error(error);
  });
}
//terminar el edit
function FinishEdit() {
  remove(ref(database, "posts/" + thekey))
  writeNewPost()
  location.reload(true)
}
//borrar el post
function ClearPost(Postkey) {
  console.log("BORRANDO post: " + Postkey);
  remove(ref(database, "posts/" + Postkey));
  location.reload(true)
}